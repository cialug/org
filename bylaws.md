# Please note that these are woefully out of date and we hardly follow them at all!

# Article I – Name and Purpose

## Section One:

The name of this organization shall be the “Central Iowa Linux Users Group” (CIALUG).

## Section Two:

The purpose of this organization shall be to promote the Linux operating system and Open Source software.

# Article II – Membership

## Section One:

A person shall be a member of this organization upon paying the yearly dues at a regular meeting. Their membership shall last one calendar year. The dues shall be set at the second regular meeting of the year by vote of the entire organization.

## Section Two:

A member shall be expelled from this organization upon recommendation of 50% of the Executive Committee and upon a 75% vote of the current full members of this organization. An expelled member shall forfit the remainder of the year’s dues and shall not be eligible for membership in this organization for one calendar year. In addition, the expelled member must have a vote of 50% of the entire organization to regain membership in this organization.

# Article III – Executive Committee

## Section One:

The executive committee shall be responsible for the day to day operation of this organization.

## Section Two:

The executive committee shall consist of the President, the Vice-President, the Treasurer, the Secretary, and the Sergeant-at-Arms.

## Section Three:

The President shall be responsible for running the meetings of this organization.

## Section Four:

The Vice-President shall be responsible for the running of the meetings in the absence of the President.

## Section Five:

The Treasurer shall be responsible for the collection and execution of all fiduciary matters of this organization. The Treasurer shall also be responsible for keeping a current list of all members of this organization.

## Section Six:

The Secretary shall be responsible for all communications pertaining to this organization. The Secretary shall be responsible for keeping and publishing the minutes of the meetings of this organization.

## Section Seven:

The Sergeant-at-Arms shall be responsible for keeping order at all meetings and functions of this organization.

## Section Eight:

The election of the executive committee shall occur as the first matter of business at the second regular meeting of the year. The executive committee shall take office at the third regular meeting of the year.

# Article IV – Voting

## Section One:

In matters of expulsion or the election of officers, 75% of the full membership shall be considered a quorum. In all other matters not specified, 50% of the full membership shall be considered a quorum. In the executive committee, a quorum shall be all members of that committee.

## Section Two:

In matters not specified, a greater than 50% vote shall be considered enough to pass the issue.

## Section Three:

All expenditures of monies greater than twenty dollars shall be passed by a majority vote of the quorum of the entire membership.

# Article V – Amendments

## Section One:

a proposal offered as an amendment to these bylaws shall have to be passed by 50% of the executive committee and 50% of the entire organization to be considered an amendment.

# Article VI – Ratification

## Section One:

These bylaws shall be ratified upon the vote of the majority of full members present at the meeting of the Central Iowa Linux Users Group on 23 June 1999.
